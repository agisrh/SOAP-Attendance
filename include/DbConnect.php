<?php
//Constants to connect with the database local
define('DB_USERNAME_LOCAL', 'root');
define('DB_PASSWORD_LOCAL', '');
define('DB_HOST_LOCAL', 'localhost');
define('DB_NAME_LOCAL', 'db_absen');

//Constants to connect with the database server
define('DB_USERNAME_SERVER', '');
define('DB_PASSWORD_SERVER', '');
define('DB_HOST_SERVER', '');
define('DB_NAME_SERVER', '');
class DbConnect
{
    //Variable to store database link
    private $con_db1;
    private $con_db2;

    //This method will connect to the database
    function connect()
    {
        //connecting to mysql database
        $this->con_db1 = new mysqli(DB_HOST_LOCAL, DB_USERNAME_LOCAL, DB_PASSWORD_LOCAL, DB_NAME_LOCAL);
        $this->con_db2 = new mysqli(DB_HOST_SERVER, DB_USERNAME_SERVER, DB_PASSWORD_SERVER, DB_NAME_SERVER);

        //Checking if any error occured while connecting
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        //finally returning the connection link
        return ['db1'=>$this->con_db1,'db2'=>$this->con_db2];
    }

}

