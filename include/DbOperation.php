<?php

class DbOperation
{
    private $con;

    function __construct()
    {
        require_once dirname(__FILE__) . '/DbConnect.php';
        $db = new DbConnect();
        $connections = $db->connect();
        $this->db1 = $connections['db1']; // Local 
        $this->db2 = $connections['db2']; // Server
    }


    public function ParseData($data,$p1,$p2){
        $data=" ".$data;
        $hasil="";
        $awal=strpos($data,$p1);
        if($awal!=""){
            $akhir=strpos(strstr($data,$p1),$p2);
            if($akhir!=""){
                $hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
            }
        }
        return $hasil;  
    }


    public function logExist($pin, $date, $time, $status){
        $stmt = $this->db2->prepare("SELECT fingerId FROM tb_attlog WHERE fingerId='$pin' AND dateAt='$date' AND timeAt='$time' AND status='$status'");
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    public function log($pin, $date, $time, $ip_address, $verified, $status){
         if (!$this->logExist($pin, $date, $time, $status)) {
            $stmt = $this->db2->prepare("INSERT INTO tb_attlog VALUES ('$pin', '$date', '$time', '$verified', '$status', '$ip_address')");
            $result = $stmt->execute();
            $stmt->close();

        }
    }

    public function dataExist($pin, $date){
        // 1 untuk Total
        if(substr($pin,0,1)=='1'){
            $stmt = $this->db2->prepare("SELECT rldate FROM rltransdata WHERE SUBSTRING_INDEX(rlcode,'.',1)='369' AND CONCAT('1', SUBSTRING_INDEX(rlcode,'.',-1))='$pin' AND rldate='$date'");
            $stmt->execute();
            $stmt->store_result();
            $num_rows = $stmt->num_rows;
            $stmt->close();
            return $num_rows > 0;

        // 2 untuk PDA
        }elseif(substr($pin,0,1)=='2'){
            $stmt = $this->db2->prepare("SELECT rldate FROM rltransdata WHERE SUBSTRING_INDEX(rlcode,'.',1)='PDA' AND CONCAT('2', SUBSTRING_INDEX(rlcode,'.',-1))='$pin' AND rldate='$date'");

            $stmt->execute();
            $stmt->store_result();
            $num_rows = $stmt->num_rows;
            $stmt->close();
            return $num_rows > 0;

        // 3 untuk J2 (SIN)
        }elseif(substr($pin,0,1)=='3'){
            $stmt = $this->db2->prepare("SELECT rldate FROM rltransdata WHERE SUBSTRING_INDEX(rlcode,'.',3)='SIN' AND CONCAT('3', SUBSTRING_INDEX(rlcode,'.',-1))='$pin' AND rldate='$date'");

            $stmt->execute();
            $stmt->store_result();
            $num_rows = $stmt->num_rows;
            $stmt->close();
            return $num_rows > 0;

        // 4 untuk kurier.id (469)
        }elseif(substr($pin,0,1)=='4'){
            $stmt = $this->db2->prepare("SELECT rldate FROM rltransdata WHERE SUBSTRING_INDEX(rlcode,'.',3)='469' AND CONCAT('4', SUBSTRING_INDEX(rlcode,'.',-1))='$pin' AND rldate='$date'");

            $stmt->execute();
            $stmt->store_result();
            $num_rows = $stmt->num_rows;
            $stmt->close();
            return $num_rows > 0;
        }
        
    }

    public function insert($pin, $date, $time, $ip_address, $verified, $status){
         if (!$this->dataExist($pin, $date) AND $status=='0') {

            // 1 untuk Total
            if(substr($pin,0,1)=='1'){
                $stmt = $this->db2->prepare("INSERT INTO rltransdata VALUES ((SELECT rlcode FROM rlempl WHERE SUBSTRING_INDEX(rlcode,'.',1)='369' AND CONCAT('1', SUBSTRING_INDEX(rlcode,'.',-1))='$pin'), '$date', '$time', NULL, NULL, '$ip_address', 'm')");
                $result = $stmt->execute();
                $stmt->close();

            // 2 untuk PDA
            }elseif(substr($pin,0,1)=='2'){
                $stmt = $this->db2->prepare("INSERT INTO rltransdata VALUES ((SELECT rlcode FROM rlempl WHERE SUBSTRING_INDEX(rlcode,'.',1)='PDA' AND CONCAT('2', SUBSTRING_INDEX(rlcode,'.',-1))='$pin'), '$date', '$time', NULL, NULL, '$ip_address', 'm')");
                $result = $stmt->execute();
                $stmt->close();

            // 3 untuk J2 (SIN)
            }elseif(substr($pin,0,1)=='3'){
                $stmt = $this->db2->prepare("INSERT INTO rltransdata VALUES ((SELECT rlcode FROM rlempl WHERE SUBSTRING_INDEX(rlcode,'.',1)='SIN' AND CONCAT('3', SUBSTRING_INDEX(rlcode,'.',-1))='$pin'), '$date', '$time', NULL, NULL, '$ip_address', 'm')");
                $result = $stmt->execute();
                $stmt->close();

            // 4 untuk kurier.id (469)
            }elseif(substr($pin,0,1)=='4'){
                $stmt = $this->db2->prepare("INSERT INTO rltransdata VALUES ((SELECT rlcode FROM rlempl WHERE SUBSTRING_INDEX(rlcode,'.',1)='469' AND CONCAT('4', SUBSTRING_INDEX(rlcode,'.',-1))='$pin'), '$date', '$time', NULL, NULL, '$ip_address', 'm')");
                $result = $stmt->execute();
                $stmt->close();

            // 5 untuk tora (269)
            }elseif(substr($pin,0,1)=='5'){
                $stmt = $this->db2->prepare("INSERT INTO rltransdata VALUES ((SELECT rlcode FROM rlempl WHERE SUBSTRING_INDEX(rlcode,'.',1)='269' AND CONCAT('5', SUBSTRING_INDEX(rlcode,'.',-1))='$pin'), '$date', '$time', NULL, NULL, '$ip_address', 'm')");
                $result = $stmt->execute();
                $stmt->close();
            }


        }elseif($status=='1'){
            // 1 untuk Total
            if(substr($pin,0,1)=='1'){
                $stmt = $this->db2->prepare("UPDATE rltransdata SET rlkeluar='$time' WHERE rlcode=(SELECT rlcode FROM rlempl WHERE SUBSTRING_INDEX(rlcode,'.',1)='369' AND CONCAT('1', SUBSTRING_INDEX(rlcode,'.',-1))='$pin') AND (rldate BETWEEN DATE_SUB('$date', INTERVAL 1 DAY)
                      AND '$date' AND rlkeluar IS NULL)");
                $result = $stmt->execute();
                $stmt->close();

            // 2 untuk PDA
            }elseif(substr($pin,0,1)=='2'){
                $stmt = $this->db2->prepare("UPDATE rltransdata SET rlkeluar='$time' WHERE rlcode=(SELECT rlcode FROM rlempl WHERE SUBSTRING_INDEX(rlcode,'.',1)='PDA' AND CONCAT('2', SUBSTRING_INDEX(rlcode,'.',-1))='$pin') AND (rldate BETWEEN DATE_SUB('$date', INTERVAL 1 DAY)
                      AND '$date' AND rlkeluar IS NULL)");
                $result = $stmt->execute();
                $stmt->close();

            // 3 untuk J2 (SIN)
            }elseif(substr($pin,0,1)=='3'){
                $stmt = $this->db2->prepare("UPDATE rltransdata SET rlkeluar='$time' WHERE rlcode=(SELECT rlcode FROM rlempl WHERE SUBSTRING_INDEX(rlcode,'.',1)='SIN' AND CONCAT('3', SUBSTRING_INDEX(rlcode,'.',-1))='$pin') AND (rldate BETWEEN DATE_SUB('$date', INTERVAL 1 DAY)
                      AND '$date' AND rlkeluar IS NULL)");
                $result = $stmt->execute();
                $stmt->close();

            // 4 untuk kurier.id (469)
            }elseif(substr($pin,0,1)=='4'){
                $stmt = $this->db2->prepare("UPDATE rltransdata SET rlkeluar='$time' WHERE rlcode=(SELECT rlcode FROM rlempl WHERE SUBSTRING_INDEX(rlcode,'.',1)='469' AND CONCAT('4', SUBSTRING_INDEX(rlcode,'.',-1))='$pin') AND (rldate BETWEEN DATE_SUB('$date', INTERVAL 1 DAY)
                      AND '$date' AND rlkeluar IS NULL)");
                $result = $stmt->execute();
                $stmt->close();

            // 5 untuk tora (269)
            }elseif(substr($pin,0,1)=='5'){
                $stmt = $this->db2->prepare("UPDATE rltransdata SET rlkeluar='$time' WHERE rlcode=(SELECT rlcode FROM rlempl WHERE SUBSTRING_INDEX(rlcode,'.',1)='269' AND CONCAT('5', SUBSTRING_INDEX(rlcode,'.',-1))='$pin') AND (rldate BETWEEN DATE_SUB('$date', INTERVAL 1 DAY)
                      AND '$date' AND rlkeluar IS NULL)");
                $result = $stmt->execute();
                $stmt->close();

            }
        }

        if ($result) {
            return 1;
        } else {
            return 0;
        }
    }

   
}

